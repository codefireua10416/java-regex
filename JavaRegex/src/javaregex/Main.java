/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaregex;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String line = "I'm Vasya Pupkin and i'm 21 years old";
        
        System.out.println(line.matches(".*\\d{1,2}.*"));
        
        System.out.println(line.replaceAll("[aA]", "_").replaceAll("[mM]", "+").replaceAll("[oO]", "-"));
        System.out.println(line);
        
    }
    
}
