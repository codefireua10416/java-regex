/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaregex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String line = "I'm Vasya Pupkin. My phone number is 0 (67) 123-45-67,  +380677654321.";
        
        // +380 (67) 123-45-67
        Pattern compile = Pattern.compile("\\+?(?<c>\\d{3})?\\s*\\(?(?<o>\\d{2})\\)?\\s*(?<n>\\d{3}-?\\d{2}-?\\d{2})");
        
        Matcher matcher = compile.matcher(line);
        
        while (matcher.find()) {
            System.out.println("Found_________________");
//            System.out.println(matcher.group());
            
            String country = matcher.group("c");
            String operator = matcher.group("o");
            String number = matcher.group("n").replace("-", "");
            
//            System.out.println("Country: " + country);
//            System.out.println("Operator: " + operator);
//            System.out.println("Number: " + number);
            
            
            System.out.printf("+%3s (%2s) %7s\n", country, operator, number);
        }
    }
    
}
